package com.stackstate.assessment

import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.FunSpec
import org.scalatest.prop.PropertyChecks

object Generators {

  val vertex: Gen[Component] =
    for {
      label <- Gen.alphaLowerChar.map(_.toString)
      failureRate <- Gen.choose(0, 100)
    } yield {
      val comp = Component(label)
      comp.failureRate = failureRate
      comp
    }

  val verticesGenerator = Gen.sized { size =>
    Gen.containerOfN[Set, Component](size, vertex)
  }

  def singleEdge(vertices: Seq[Component]) =
    for {
      sourceVertex <- Gen.oneOf(vertices)
      endVertex <- Gen.oneOf(vertices).retryUntil(_ != sourceVertex)
    } yield (sourceVertex, endVertex)

  def edges(vertices: Seq[Component]) = Gen.sized { size =>
    Gen.containerOfN[Set, (Component, Component)](size, singleEdge(vertices))
  }

  val graphGenerator = Gen.sized { size =>
    for {
      vertices <- Gen.resize(size, verticesGenerator)
      edges <- edges(vertices.toSeq)
    } yield ComponentGraph(vertices, edges)
  }

  implicit val gen = Arbitrary(graphGenerator)
}
class GraphSpec extends FunSpec with PropertyChecks {
  import Generators._

  val graph: Graph[Component] = ComponentGraph(
    vertices = Set(
      Component("a"),
      Component("b"),
      Component("c"),
      Component("d"),
      Component("e"),
      Component("f"),
      Component("g")
    ),
    edges = Set(
      Component("a") -> Component("b"),
      Component("b") -> Component("c"),
      Component("b") -> Component("d"),
      Component("e") -> Component("f")
    )
  )

  val singleVertexEdgelessGraph = ComponentGraph(
    vertices = Set(
      Component("a")
    ),
    edges = Set()
  )

  val twoVerticesEdgelessGraph = ComponentGraph(
    vertices = Set(
      Component("a"),
      Component("b")
    ),
    edges = Set()
  )

  val cycleGraph = ComponentGraph(
    vertices = Set(
      new Component("a", 0.7),
      new Component("b", 0.4),
      new Component("c", 0.9),
      new Component("d", 0.2)
    ),
    edges = Set(
      Component("a") -> Component("b"),
      Component("b") -> Component("c"),
      Component("c") -> Component("d"),
      Component("d") -> Component("a")
    )
  )

  val aPathGraph = ComponentGraph(
    vertices = Set(
      Component("a"),
      Component("b"),
      Component("c"),
      Component("d")
    ),
    edges = Set(
      Component("a") -> Component("b"),
      Component("c") -> Component("d")
    )
  )

  val stronglyConnectedGraph = ComponentGraph(
    vertices = Set(
      Component("a"),
      Component("b"),
      Component("c"),
      Component("d"),
      Component("e")
    ),
    edges = Set(
      Component("a") -> Component("b"),
      Component("b") -> Component("c"),
      Component("c") -> Component("d"),
      Component("c") -> Component("e"),
      Component("d") -> Component("c"),
      Component("e") -> Component("a")
    )
  )

  describe("A Graph") {
    describe("edgesOf") {
      it("retuns None for an inexistent vertex") {
        assert(graph.edgesOf(Component("h")) === None)
      }

      it("returns an empty Set for a vertex without edges") {
        assert(graph.edgesOf(Component("g")) === Some(Set()))
      }

      it("returns a set of vertices that are linked with a given vertex") {
        assert(graph.edgesOf(Component("b")) === Some(Set(Component("c"), Component("d"))))
      }
    }

    describe("isConnected") {
      it("is connected when there is only a vertex") {
        assert(singleVertexEdgelessGraph.isConnected)
      }

      it("is not connected when there are two vertices and no edges") {
        assert(!twoVerticesEdgelessGraph.isConnected)
      }

      it("is connected when graph is a cycle") {
        assert(cycleGraph.isConnected)
      }

      it("is not connected when there isn't a path between every vertex") {
        assert(!graph.isConnected)
      }
    }

    describe("isStronglyConnected") {
      it("is strongly connected when there is only a vertex") {
        assert(singleVertexEdgelessGraph.isStronglyConnected)
      }

      it("is not connected when there are two vertices and no edges") {
        assert(!twoVerticesEdgelessGraph.isStronglyConnected)
      }

      it("is connected when graph is a cycle") {
        assert(cycleGraph.isStronglyConnected)
      }

      it("is not connected when there isn't a path between every vertex") {
        assert(!graph.isStronglyConnected)
      }

      it("is strongly connected") {
        assert(stronglyConnectedGraph.isStronglyConnected)
      }
    }

    describe("removeVertex") {
      val v = Component("c")
      val g = stronglyConnectedGraph.removeVertex(v)

      it("removes the vertex v from the set of vertices") {
        assert(g.vertices === Set(Component("a"), Component("b"), Component("d"), Component("e")))
      }

      it("removes any edges that are linked to v") {
        assert(g.edges === Set(
          Component("a") -> Component("b"),
          Component("e") -> Component("a")
        ))
      }

      it("is aliased to the operator -") {
        assert(stronglyConnectedGraph - v === stronglyConnectedGraph.removeVertex(v))
      }
    }

    describe("sortVertices") {
      it("returns a sequence vertices of the graph sorted by a given property") {
        forAll("graph") { (graph: ComponentGraph) =>
          val sorted = graph.sortByFailureRate()
          whenever(sorted.nonEmpty) {
            (sorted.size - 1 to 0).forall(n => sorted(n).failureRate <= sorted(n - 1).failureRate)
          }
        }
      }
    }

    describe("isCutVertex") {
      it("is a true when G - v is disconnected") {
        assert(stronglyConnectedGraph.isCutVertex(Component("c")))
      }

      it("is false otherwise") {
        assert(!stronglyConnectedGraph.isCutVertex(Component("a")))
      }
    }

    describe("connectedComponents") {
      val g = ComponentGraph(
        vertices = Set(
          Component("a"),
          Component("b"),
          Component("c"),
          Component("d"),
          Component("e"),
          Component("f"),
          Component("g"),
          Component("h")
        ),
        edges = Set(
          Component("a") -> Component("b"),
          Component("a") -> Component("f"),
          Component("b") -> Component("c"),
          Component("b") -> Component("f"),
          Component("c") -> Component("d"),
          Component("c") -> Component("g"),
          Component("e") -> Component("a"),
          Component("f") -> Component("e"),
          Component("f") -> Component("g"),
          Component("g") -> Component("c"),
          Component("h") -> Component("g")
        )
      )

      it("constructs a set of connected components of a graph") {
        assert(g.connectedComponents === Set(
          ComponentGraph(
            vertices = Set(Component("h")),
            edges = Set()
          ),
          ComponentGraph(
            vertices = Set(Component("d")),
            edges = Set()
          ),
          ComponentGraph(
            vertices = Set(
              Component("a"),
              Component("e"),
              Component("f"),
              Component("b")
            ),
            edges = Set(
              Component("a") -> Component("b"),
              Component("a") -> Component("f"),
              Component("b") -> Component("f"),
              Component("e") -> Component("a"),
              Component("f") -> Component("e")
            )
          ),
          ComponentGraph(
            vertices = Set(
              Component("c"),
              Component("g")
            ),
            edges = Set(
              Component("c") -> Component("g"),
              Component("g") -> Component("c")
            )
          )
        ))
      }
    }
  }
}
