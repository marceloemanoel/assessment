package com.stackstate.assessment

import scala.annotation.tailrec

trait Graph[Vertex] {
  /**
    * Combination of two vertices
    */
  type Edge = (Vertex, Vertex)

  /**
    * A sequence of vertices used to traverse the graph
    */
  type Path = Seq[Vertex]

  /**
    * The given vertices of this graph.
    */
  val vertices: Set[Vertex]

  /**
    * The given edges of this graph.
    */
  val edges: Set[Edge]

  /**
    * Represents the graph using an adjacency list
    */
  private lazy val representation: Map[Vertex, Set[Vertex]] = {
    vertices.map(v => v -> edges.filter(_._1 == v).map(_._2))
            .toMap
  }

  /**
    * Returns an optional set of vertices that are connected to a given vertex.
    *
    * @param vertex Any vertex of this graph
    * @return Some(Set[Vertex]) if the given vertex belongs to this graph or None otherwise.
    */
  def edgesOf(vertex: Vertex): Option[Set[Vertex]] = representation.get(vertex)

  /**
    * Returns the number of edges that are connected with the given vertex.
    *
    * @param vertex Any vertex of this graph
    * @return Some(Int) if the given vertex belongs to this graph or None otherwise.
    */
  def degreeOf(vertex: Vertex): Option[Int] = edgesOf(vertex).map(_.size)

  /**
    * A graph is connected if there is a path between every pair of vertices.
    * @return true if this graph is connected, false otherwise.
    */
  def isConnected: Boolean =
    vertices.forall(vertex => traverse(vertex).size == vertices.size)

  /**
    * Traverse the graph using DFS traversal algorithm starting from an arbitrary node
    * @param from traversal's starting point
    * @return the graph traversal path
    */
  def traverse(from: Vertex): Path = {
    def notVisited(vertex: Vertex, path: Path): Seq[Vertex] = {
      edgesOf(vertex).map(vertices => vertices.filterNot(path.contains(_)))
                    .map(_.toSeq)
                    .getOrElse(Nil)
    }

    @tailrec
    def loop(remaining: Seq[Vertex], path: Path): Path = {
      if(remaining.isEmpty)
        path
      else
        loop(notVisited(remaining.head, path) ++ remaining.tail, path :+ remaining.head)
    }

    loop(Seq(from), Nil)
  }

  /**
    * Detects if a graph is strongly connected.
    * A graph is said to be strongly connected if from any pair of vertices v and w there is a path between them.
    * This algorithm is based on Kasuru's.
    * @return
    */
  def isStronglyConnected: Boolean = {

    def nonVisitedVertices(acc: Set[Vertex], vertex: Vertex): Set[Vertex] =
      acc - vertex

    val firstTraversal = traverse(from = vertices.head)
    val didNotVisitVertices = firstTraversal.foldLeft(vertices)(nonVisitedVertices)

    if (didNotVisitVertices.isEmpty) {
      val transposedGraph = transpose()
      val path = transposedGraph.traverse(from = vertices.head)
      val didNotVisitVerticesInTransposedGraph = path.foldLeft(vertices)(nonVisitedVertices)
      didNotVisitVerticesInTransposedGraph.isEmpty
    }
    else {
      false
    }
  }

  /**
    * Constructs a graph with the given vertices and edges.
    * @param vertices The vertices of the new graph.
    * @param edges The edges of the new graph
    */
  def apply(vertices: Set[Vertex], edges: Set[Edge]): Graph[Vertex]

  /**
    * Constructs the transposed graph inverting all the edges
    */
  def transpose(): Graph[Vertex] =
    apply(vertices, edges.map(pair => (pair._2, pair._1)))

  /**
    * Sort the vertices of this graph according to a given comparator function
    * @param f a function that compare two vertices v1 and v2. If it returns true then v1 >= v2 otherwise v1 < v2
    * @return A Seq[Vertex] sorted according to the given function
    */
  def sortVertices(f: (Vertex, Vertex) => Boolean): Seq[Vertex] =
    vertices.toSeq.sortWith(f)

  /**
    * Constructs a new graph removing a given vertex and its linked edges
    * @param v the vertex to remove
    */
  def removeVertex(v: Vertex): Graph[Vertex] =
    apply(vertices - v, edges.filterNot { case (v1, v2) => v == v1 || v == v2 })

  /**
    * Alias function to `removeVertex`.
    * @param v The vertex to remove from this graph
    * @return A new graph without the given vertex and its linked edges
    */
  def -(v: Vertex): Graph[Vertex] = removeVertex(v)

  /**
    * Prints the representation in the form:
    * ```
    * v1 -> [v2, v3]
    * v2 -> []
    * v3 -> [v1, v4]
    * v4 -> []
    * ```
    * @return
    */
  override def toString: String = {
    representation.map { case (vertex, connections) =>
      s"$vertex -> $connections"
    }.toSeq.mkString("\n")
  }

  /**
    * Determines if the given vertex is a cutting vertex.
    * If the removal of the given vertex produces more connected components than the current graph does,
    * then this vertex is a cut vertex.
    *
    * @param v A given vertex
    * @return True when the removal of this vertex produces more component components than the current graph.
    */
  def isCutVertex(v: Vertex): Boolean = removeVertex(v).connectedComponents.size > connectedComponents.size

  /**
    * Returns the connect components of this graph using a slight modification of Kosaraju's algorithm:
    *
    * ```
    *   For each vertex u of the graph, mark u as unvisited. Let L be empty.
    *   For each vertex u of the graph do Visit(u), where Visit(u) is the recursive subroutine:
    *   If u is unvisited then:
    *   Mark u as visited.
    *   For each out-neighbour v of u, do Visit(v).
    *   Prepend u to L.
    *   Otherwise do nothing.
    *
    *   For each element u of L in order, do Assign(u,u) where Assign(u,root) is the recursive subroutine:
    *   If u has not been assigned to a component then:
    *   Assign u as belonging to the component whose root is root.
    *   For each in-neighbour v of u, do Assign(v,root).
    *   Otherwise do nothing.
    *```
    * Time Complexity - O(V+E)
    * @return
    */
  def connectedComponents: Set[Graph[Vertex]] = {
    def notVisited(v: Vertex, visited: Set[Vertex]): Seq[Vertex] = {
      edgesOf(v).map(vertices => vertices.filterNot(visited.contains(_)))
                .map(_.toSeq)
                .getOrElse(Nil)
    }

    @tailrec
    def dfs(remaining: Seq[Vertex], stack: List[Vertex], visited: Set[Vertex]): List[Vertex] = {
      if(remaining.isEmpty) {
        stack
      }
      else {
        val children = notVisited(remaining.head, visited)
        val nextStack = if (children.isEmpty) stack :+ remaining.head else stack

        dfs(
          children ++ remaining.tail,
          nextStack,
          visited + remaining.head
        )
      }
    }
    val stack = dfs(vertices.toSeq, List(), Set())

//    transpose().dfs(vertices.toSeq, stack, Set())
    ???
  }

}
