package com.stackstate.assessment

case class Component(label: String) {
  var failureRate: Double = 0

  def this(lbl: String, failureRate: Double = 0) {
    this(lbl)
    this.failureRate = failureRate
  }

  override def toString: String = label
}

case class ComponentGraph(vertices: Set[Component], edges: Set[(Component, Component)])
     extends Graph[Component] {

  /**
    * Constructs a graph with the given vertices and edges
    * @param vertices The vertices of the new graph.
    * @param edges The edges of the new graph
    * @return
    */
  override def apply(vertices: Set[Component], edges: Set[(Component, Component)]): Graph[Component] = {
    ComponentGraph(vertices, edges)
  }

  /**
    * Sort the vertices by failure rate
    * @return
    */
  def sortByFailureRate() = sortVertices { case (v1, v2) =>
    v1.failureRate > v2.failureRate
  }

  /**
    * Sorts the vertices of this graph according to the following order of priority:
    * Cuts vertices then by failure rate
    *
    * @return
    */
  def verticesSortedByImportance(): Seq[Component] = {
    def cutsFirst(v1: Component, v2: Component) = {
      isCutVertex(v1) && !isCutVertex(v2)
    }

    sortByFailureRate().sortWith(cutsFirst)
  }
}
