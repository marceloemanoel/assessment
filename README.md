# Coding Assessment StackState

## Background

In this assessment you will code something related to StackState. 

As you may know StackState has a dependency graph of IT stack components. The nodes in the graph are called components. Like with any other IT stack there may be cyclic dependencies. Components can produce many events related to the health of that component. An example of an event might be some anomaly that was detected in the memory usage of a server. That event may or may not be of importance to an operator. However, how does one determine whether an event is important enough to respond to? Let us assume for this exercise that it depends solely on a threshold that depends on the importance of the component. The next question is becomes how does one calculate the importance of components relative to other components? This is the question you need to answer with a piece of working code.

## Calculating the importance heuristic

You can come up with any calculation you want, but here are three measures that may be of interest:

1. As you may know there are different algorithms out there that can be used to determine the relative connectedness of nodes in a graph. Connectedness may be one measure. 

1. Another measure is that some components may be rated as more important than others, but of course we cannot ask the user to rate each and every component.  

1. Another factor may be the failure rate. A component that causes failures more often than other components is more likely to cause events that need intervention than others. It can be assumed that the failure rate is known.

## Requirements

Your code should demonstrate the effectiveness of the component importance calculation that you came up with. There are no requirements on how you demonstrate this.

The code you produce should be written in Scala, should not depend on graph libraries and should be optimized for readability. There are no other non-functional requirements. 

Please note: there is always room for improvement, but please timebox this exercise to a duration you feel comfortable with. Any insights in terms of refactorings or refinements can be explained when you come over to demonstrate your code. 

If you have any questions don't hesitate to ask: lbogaards@stackstate.com/+31653639729.